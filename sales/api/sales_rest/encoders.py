from .models import SalesPerson, PotentialCustomer, AutoDetailVO, SalesRecord
from common.json import ModelEncoder


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ['name', 'employee_number']


class PotentialCustomerEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = ['name', 'address', 'phone_number']


class AutoDetailVOEncoder(ModelEncoder):
    model = AutoDetailVO
    properties = ['href', 'vin', 'model', 'color']


class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = ['automobile', 'sales_person', 'customer', 'price']
    encoders = {
        "sales_person": SalesPersonEncoder(),
        "automobile": AutoDetailVOEncoder(),
        'customer': PotentialCustomerEncoder(),
        }
