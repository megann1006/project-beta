from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import SalesPerson, PotentialCustomer, SalesRecord, AutoDetailVO
from .encoders import SalesPersonEncoder, PotentialCustomerEncoder, SalesRecordEncoder, AutoDetailVOEncoder


@require_http_methods(["GET", "POST"])
def list_sales_person(request):
    if request.method == "GET":
        sales_person = SalesPerson.objects.all()
        return JsonResponse({"sales_person": sales_person}, encoder=SalesPersonEncoder, safe=False)
    else:
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(sales_person, encoder=SalesPersonEncoder, safe=False)


@require_http_methods(["GET", "PUT", "DELETE"])
def show_sales_person(request, pk):
    if request.method == "GET":
        sales_person = SalesPerson.objects.get(pk=pk)
        return JsonResponse({"sales_person": sales_person}, encoder=SalesPersonEncoder, safe=False)
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            sales_person = SalesPerson.objects.get(pk=content["sales_person"])
            content["sales_person"] = sales_person
        except SalesPerson.DoesNotExist:
            return JsonResponse({"error": "Sales person does not exist"}, status=400)
    else:
        count, _ = SalesPerson.objects.filter(pk=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def list_potential_customer(request):
    if request.method == "GET":
        potential_customer = PotentialCustomer.objects.all()
        return JsonResponse({"potential_customer": potential_customer}, encoder=PotentialCustomerEncoder, safe=False)
    else:
        content = json.loads(request.body)
        potential_customer = PotentialCustomer.objects.create(**content)
        return JsonResponse(potential_customer, encoder=PotentialCustomerEncoder, safe=False)


@require_http_methods(["GET", "PUT", "DELETE"])
def show_potential_customer(request, pk):
    if request.method == "GET":
        customer = PotentialCustomer.objects.get(pk=pk)
        return JsonResponse({"customer": customer}, encoder=PotentialCustomerEncoder, safe=False)
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            customer = PotentialCustomer.objects.get(pk=content["customer"])
            content["customer"] = customer
        except PotentialCustomer.DoesNotExist:
            return JsonResponse({"error": "Customer does not exist"}, status=400)
    else:
        count, _ = PotentialCustomer.objects.filter(pk=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def list_sales_records(request):
    if request.method == "GET":
        sales_record = SalesRecord.objects.all()
        return JsonResponse({"sales_record": sales_record}, encoder=SalesRecordEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
            vin = content['automobile']
            automobile = AutoDetailVO.objects.get(vin=vin)
            content['automobile'] = automobile
        except AutoDetailVO.DoesNotExist:
            return JsonResponse({"error": "auto issues"}, status=400)
        try:
            sales_name = content['sales_person']
            sales_person = SalesPerson.objects.get(name=sales_name)
            content['sales_person'] = sales_person
        except AutoDetailVO.DoesNotExist:
            return JsonResponse({"error": " sales issues"}, status=400)
        try:
            customer_name = content['customer']
            customer = PotentialCustomer.objects.get(name=customer_name)
            content['customer'] = customer
        except AutoDetailVO.DoesNotExist:
            return JsonResponse({"error": "customer issues"}, status=400)
        sales_record = SalesRecord.objects.create(**content)
        return JsonResponse(sales_record, encoder=SalesRecordEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def show_sales_record(request, pk):
    if request.method == "GET":
        sales_record = SalesRecord.objects.get(pk=pk)
        return JsonResponse(sales_record, encoder=SalesRecordEncoder, safe=False)
    else:
        count, _ = SalesRecord.objects.filter(pk=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET"])
def get_salespersons_record(request, pk):
    if request.method == "GET":
        try:
            sales = SalesRecord.objects.filter(sales_person=pk)
            return JsonResponse({"sales": sales}, encoder=SalesRecordEncoder, safe=False)
        except SalesRecord.DoesNotExist:
            return JsonResponse({"error": "Sales record does not exist"}, status=400)


@require_http_methods(["GET"])
def car_inventory(request):
    cars_sold = []
    for car in SalesRecord.objects.all():
        cars_sold.append(car.automobile.vin)
    present = AutoDetailVO.objects.exclude(vin__in=cars_sold)
    return JsonResponse({"cars": present}, encoder=AutoDetailVOEncoder, safe=False)
