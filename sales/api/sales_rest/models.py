from django.db import models
from django.urls import reverse


class AutoDetailVO(models.Model):
    model = models.CharField(max_length=100)
    vin = models.CharField(max_length=17, unique=True)
    color = models.CharField(max_length=50)
    href = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.vin


class SalesPerson(models.Model):
    name = models.CharField(max_length=100, unique=True)
    employee_number = models.IntegerField(unique=True)


class PotentialCustomer(models.Model):
    name = models.CharField(max_length=100, unique=True)
    address = models.CharField(max_length=200, unique=True)
    phone_number = models.IntegerField(unique=True)


class SalesRecord(models.Model):
    automobile = models.ForeignKey(AutoDetailVO, related_name="salesrecord", on_delete=models.PROTECT)
    sales_person = models.ForeignKey(SalesPerson, related_name="salesrecord", on_delete=models.PROTECT)
    customer = models.ForeignKey(PotentialCustomer, related_name="salesrecord", on_delete=models.PROTECT)
    price = models.PositiveBigIntegerField()

    def __str__(self):
        return f'{self.automobile} sold to {self.customer} by {self.sales_person}'

    def get_api_url(self):
        return reverse("show_sales_record", kwargs={"pk": self.pk})
