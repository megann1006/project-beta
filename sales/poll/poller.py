import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

from sales_rest.models import AutoDetailVO
# Import models from sales_rest, here.
# from sales_rest.models import Something


def poll():
    while True:
        print('Sales poller polling for data')
        try:
            url = "http://inventory-api:8000/api/automobiles/"
            response = requests.get(url)
            content = json.loads(response.content)

            for automobile in content['autos']:
                AutoDetailVO.objects.update_or_create(
                    href=automobile['href'],
                    defaults={'vin': automobile['vin'],
                              'model': automobile['model']['name'],
                              'color': automobile['color'],
                              }
                )
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
