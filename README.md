# CarCar Inventory Management Web Application

## Developers:
Megan Rodriguez, Matthew Bowes

## Intended Market
CarCar is a comprehensive inventory management web application designed specifically for car dealerships. It provides essential features to help dealerships effectively manage their inventory, record sales, and streamline customer, salesperson, technician, and service appointment management.

## Functionality
CarCar offers the following key functionalities:

Home Page: Displays a dynamic slideshow showcasing some of the cars in inventory.

In the Nav:

Manufacturers: Provides access for users to view and add new manufacturers.

Vehicle Models: Provides access for users to view and add new vehicle models.

Automobiles: Provides access for users to view and add new automobiles.

Technicians: Provides access for users to view and add new technicians.

Appointments: Provides access for users to view appointments and appointment history and add new appointments.

Sales Person: Provides access for users to view sales person and sales person history and add new sales people.

Customers: Provides access for users to view and add new customers.

Sales: Provides access for users to view and add new sales.

## Project Initialization
To set up and use the CarCar application, please follow the steps below:

Clone the repository project at:
https://gitlab.com/sjp19-public-resources/sjp-2022-april/project-beta

*Type this in your terminal to run docker

docker volume create beta-data

docker-compose build

docker-compose up

*Confirm that all  containers are running

Run the application at: http://localhost:3000/

Please note that CarCar utilizes Docker for seamless deployment and management.
