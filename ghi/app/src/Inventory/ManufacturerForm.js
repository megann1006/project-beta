import React, { useState, useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function ManufacturerForm() {
    const notify = () => toast("Manufacturer Added");

    const [formData, setFormData] = useState({
        name: '',
    })

    useEffect(() => {
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const manuUrl = 'http://localhost:8000/api_manufacturers/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(manuUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-manufacturer-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange}
                                value={formData.name}
                                placeholder="Name"
                                required type="text"
                                name="name" id="name"
                                className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <button className="btn btn-primary" onClick={notify}>Create</button>
                        <ToastContainer
                            position="top-right"
                            autoClose={2000}
                            hideProgressBar={false}
                            newestOnTop={false}
                            closeOnClick
                            rtl={false}
                            pauseOnFocusLoss
                            draggable
                            pauseOnHover={false}
                            theme="light"
                        />
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ManufacturerForm;
