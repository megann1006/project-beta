import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerForm from './Inventory/ManufacturerForm';
import ManufacturerList from './Inventory/ManufacturerList';
import VehicleModelList from './Inventory/VehicleModelList';
import VehicleModelForm from './Inventory/VehicleModelForm';
import AutomobileList from './Inventory/AutomobileList';
import AutomobileForm from './Inventory/AutomobileForm';
import TechnicianList from './Service/TechnicianList';
import TechnicianForm from './Service/TechnicianForm';
import AppointmentList from './Service/AppointmentList';
import AppointmentForm from './Service/AppointmentForm';
import ServiceHistory from './Service/ServiceHistory';
import CustomerForm from './Sales/CustomerForm';
import SalesPersonForm from './Sales/SalePersonForm';
import SaleRecordForm from './Sales/SaleRecordForm';
import SalesPersonRecordList from './Sales/SalesPersonRecordList';
import SalesRecordList from './Sales/SalesRecordList';
import EmployeeList from './Sales/EmployeeList';
import CustomerList from './Sales/CustomerList';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers">
            <Route index element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="/models">
            <Route index element={<VehicleModelList />} />
            <Route path="new" element={<VehicleModelForm />} />
          </Route>
          <Route path="/automobiles">
            <Route index element={<AutomobileList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="/technicians">
            <Route index element={<TechnicianList />} />
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="/appointments">
            <Route index element={<AppointmentList />} />
            <Route path="new" element={<AppointmentForm />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>
          <Route path="/customers">
            <Route path="new" element={<CustomerForm />} />
            <Route path="list" element={<CustomerList />} />

          </Route>
          <Route path="/sales">
          <Route index element={<SalesRecordList />} />
            <Route path="new" element={<SaleRecordForm />} />
            <Route path="record" element={<SalesPersonRecordList />} />
            <Route path="employees" element={<EmployeeList />} />
          </Route>
          <Route path="employees">
            <Route path="new" element={<SalesPersonForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
