import React, { useEffect, useState } from 'react';
import { Carousel } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCarSide } from '@fortawesome/free-solid-svg-icons';
import './MainPage.css';

function MainPage() {
  const [models, setModels] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch('http://localhost:8100/api/models/');
      const data = await response.json();
      setModels(data.models);
    };
    fetchData();
  }, []);

  return (
    <div className="px-4 py-2 my-2 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
        <h3>Browse our Vehicle Models Below</h3>
        <Carousel>
          {models.map(model => (
            <Carousel.Item key={model.id}>
              <img
                className="d-block w-100 carousel-image"
                src={model.picture_url}
                alt={model.name}
                height="400"
              />
              <Carousel.Caption>
                <h3>{model.name}</h3>
                <p>{model.manufacturer.name}</p>
              </Carousel.Caption>
            </Carousel.Item>
          ))}
        </Carousel>
      </div>
      <div class="wrapper">
        <span><FontAwesomeIcon icon={faCarSide} size="2xl" style={{color: "#274537",}} /></span>
        <span><FontAwesomeIcon icon={faCarSide} size="2xl" style={{color: "#274537",}} /></span>
        <span><FontAwesomeIcon icon={faCarSide} size="2xl" style={{color: "#274537",}} /></span>
        <span><FontAwesomeIcon icon={faCarSide} size="2xl" style={{color: "#274537",}} /></span>
        <span><FontAwesomeIcon icon={faCarSide} size="2xl" style={{color: "#274537",}} /></span>
        <span><FontAwesomeIcon icon={faCarSide} size="2xl" style={{color: "#274537",}} /></span>
        <span><FontAwesomeIcon icon={faCarSide} size="2xl" style={{color: "#274537",}} /></span>
        <span><FontAwesomeIcon icon={faCarSide} size="2xl" style={{color: "#274537",}} /></span>
        <span><FontAwesomeIcon icon={faCarSide} size="2xl" style={{color: "#274537",}} /></span>
        <span><FontAwesomeIcon icon={faCarSide} size="2xl" style={{color: "#274537",}} /></span>
      </div>
    </div>
  );
}

export default MainPage;
