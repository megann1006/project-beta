import { NavLink } from 'react-router-dom';
import { Dropdown, DropdownButton } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCarSide } from '@fortawesome/free-solid-svg-icons';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark" style={{ backgroundColor: '#396955' }}>
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar <FontAwesomeIcon icon={faCarSide} style={{color: "#ffffff",}} /></NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <DropdownButton variant="custom" id="dropdown-item-button" title="Manufacturers">
              <Dropdown.Item className="text-dark" as={NavLink} to="/manufacturers">View Manufacturers</Dropdown.Item>
              <Dropdown.Item className="text-dark" as={NavLink} to="/manufacturers/new">Add Manufacturer</Dropdown.Item>
            </DropdownButton>
            <DropdownButton variant="custom" id="dropdown-item-button" title="Vehicle Models">
              <Dropdown.Item as={NavLink} to="/models">View Vehicle Models</Dropdown.Item>
              <Dropdown.Item as={NavLink} to="/models/new">Add Vehicle Models</Dropdown.Item>
            </DropdownButton>
            <DropdownButton variant="custom" id="dropdown-item-button" title="Automobiles">
              <Dropdown.Item as={NavLink} to="/automobiles">View Automobiles</Dropdown.Item>
              <Dropdown.Item as={NavLink} to="/automobiles/new">Add Automobiles</Dropdown.Item>
            </DropdownButton>
            <DropdownButton variant="custom" id="dropdown-item-button" title="Technicians">
              <Dropdown.Item as={NavLink} to="/technicians">View Technicians</Dropdown.Item>
              <Dropdown.Item as={NavLink} to="/technicians/new">Add Technicians</Dropdown.Item>
            </DropdownButton>
            <DropdownButton variant="custom" id="dropdown-item-button" title="Appointments">
              <Dropdown.Item as={NavLink} to="/appointments/new">Add Appointments</Dropdown.Item>
              <Dropdown.Item as={NavLink} to="/appointments">View Appointments</Dropdown.Item>
              <Dropdown.Item as={NavLink} to="/appointments/history">Appointment History</Dropdown.Item>
            </DropdownButton>
            <DropdownButton variant="custom" id="dropdown-item-button" title="Sales Person">
              <Dropdown.Item as={NavLink} to="/sales/record">Sales Person History</Dropdown.Item>
              <Dropdown.Item as={NavLink} to="/employees/new">Add Sales Person</Dropdown.Item>
              <Dropdown.Item as={NavLink} to="sales/employees">View Sales People</Dropdown.Item>
            </DropdownButton>
            <DropdownButton variant="custom" id="dropdown-item-button" title="Customers">
              <Dropdown.Item as={NavLink} to="/customers/new">Add Customer</Dropdown.Item>
              <Dropdown.Item as={NavLink} to="/customers/list">View Customers</Dropdown.Item>
            </DropdownButton>
            <DropdownButton variant="custom" id="dropdown-item-button" title="Sales">
              <Dropdown.Item as={NavLink} to="/sales">View sales</Dropdown.Item>
              <Dropdown.Item as={NavLink} to="/sales/new">Add sales</Dropdown.Item>
            </DropdownButton>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
