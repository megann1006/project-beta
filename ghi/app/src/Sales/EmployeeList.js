import React, { useEffect, useState } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function EmployeeList () {
  const notify = () => toast("Employee Deleted");

    const [employees, setEmployees] = useState([])

    const getData = async () => {
        const response = await fetch("http://localhost:8090/api/employees/")
        if (response.ok) {
            const data = await response.json()
            setEmployees(data.sales_person)
        }
    }

    useEffect(() =>{
        getData()
    }, [])


    const handleDelete = async (employeeId) => {
      try {
          await fetch(`http://localhost:8090/api/employees/${employeeId}`, {
              method: 'DELETE'
          });
          setEmployees(employees.filter(employee => employee.id !== employeeId));
          notify();
      } catch (error) {
          console.error('Error deleting appointment:', error);
      }
  };

    return (
      <>
        <h1> Sales Employees </h1>
        <table className="table table-striped">
            <thead>
            <tr>
              <th>Salesperson</th>
              <th>Employee number</th>
            </tr>
          </thead>
          <tbody>
            {employees.map(employee => {
              return (
                <tr key={employee.href}>
                    <td>{employee.name}</td>
                    <td>{employee.employee_number }</td>
                    <td>
                        <button onClick={() => handleDelete(employees.id)} className="btn btn-danger">Delete</button>
                        <ToastContainer
                            position="top-right"
                            autoClose={2000}
                            hideProgressBar={false}
                            newestOnTop={false}
                            closeOnClick
                            rtl={false}
                            pauseOnFocusLoss
                            draggable
                            pauseOnHover={false}
                            theme="light"
                        />
                    </td>
                </tr>
              )
            })}
          </tbody>
        </table>
        </>
    )
}

export default EmployeeList
