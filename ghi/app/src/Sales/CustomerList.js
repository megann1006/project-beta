import React, { useEffect, useState } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function CustomerList() {
  const notify = () => toast("Customer Deleted");

  const [customers, setCustomers] = useState([])

  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/customers/")
    if (response.ok) {
      const data = await response.json()
      setCustomers(data.potential_customer)
    }
  }

  useEffect(() => {
    getData()
  }, [])


  const handleDelete = async (customerId) => {
    try {
      await fetch(`http://localhost:8090/api/customers/${customerId}`, {
        method: 'DELETE'
      });
      setCustomers(customers.filter(customer => customer.id !== customerId));
      notify();
    } catch (error) {
      console.error('Error deleting appointment:', error);
    }
    setCustomers(customers.filter(customer => customer.id !== customerId))
  }

  return (
    <>
      <h1> Potential and Current Customers </h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Phone number</th>
            <th>Address</th>
          </tr>
        </thead>
        <tbody>
          {customers.map(customer => {
            return (
              <tr key={customer.href}>
                <td>{customer.name}</td>
                <td>{customer.phone_number}</td>
                <td>{customer.address}</td>
                <td>
                  <button onClick={() => handleDelete(customers.id)} className="btn btn-danger">Delete</button>
                  <ToastContainer
                    position="top-right"
                    autoClose={2000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover={false}
                    theme="light"
                  />
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </>
  )
}

export default CustomerList
