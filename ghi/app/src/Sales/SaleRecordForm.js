import React, { useEffect, useState } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function SaleRecordForm() {
    const notify = () => toast("Sale Record Added");

    const [automobile, setAutomobile] = useState([])
    const [employee, setEmployee] = useState([])
    const [customer, setCustomer] = useState([])
    const [price, setPrice] = useState('')
    const [auto, setAuto] = useState('')
    const [emp, setEmp] = useState('')
    const [cust, setCust] = useState('')

    const fetchData = async () => {

        const autoUrl = 'http://localhost:8100/api/automobiles/'
        const response = await fetch(autoUrl)
        if (response.ok) {
            const carData = await response.json()
            setAutomobile(carData.autos)
        }

        const employeeUrl = 'http://localhost:8090/api/employees/'
        const response1 = await fetch(employeeUrl)
        if (response1.ok) {
            const employeeData = await response1.json()
            setEmployee(employeeData.sales_person)
        }

        const custUrl = 'http://localhost:8090/api/customers/'
        const response2 = await fetch(custUrl)
        if (response2.ok) {
            const customerData = await response2.json()
            setCustomer(customerData.potential_customer)
        }

    }
    useEffect(() => {
        fetchData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {};
        data.sales_person = emp;
        data.automobile = auto
        data.customer = cust;
        data.price = price;

        const url = 'http://localhost:8090/api/sales/'

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-type': 'application/json',
            }
        }

        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setAuto('')
            setEmp('')
            setCust('')
            setPrice('')
        }
    }

    const handleAutomobileChange = (event) => {
        setAuto(event.target.value)
    }
    const handleEmployeeChange = (event) => {
        setEmp(event.target.value)
    }
    const handleCustomerChange = (event) => {
        setCust(event.target.value)
    }
    const handlePriceChange = (event) => {
        setPrice(event.target.value)
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a sales record</h1>
                    <form onSubmit={handleSubmit} id="create-salesrecord-form">
                        <div className="mb-3">
                            <select onChange={handleAutomobileChange} value={auto} required name="auto" className="form-select" id="auto">
                                <option value="">Choose a automobile</option>
                                {automobile.map(autos => {
                                    return (
                                        <option key={autos.href} value={autos.vin}>{autos.model.name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleEmployeeChange} value={emp} required name="emp" className="form-select" id="emp">
                                <option value="">Choose an employee</option>
                                {employee.map(emp => {
                                    return (
                                        <option key={emp.href} value={emp.href}>{emp.name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleCustomerChange} value={cust} required name="cust" className="form-select" id="cust">
                                <option value="">Choose a customer</option>
                                {customer.map(cust => {
                                    return (
                                        <option key={cust.href} value={cust.href}>{cust.name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePriceChange} value={price} placeholder="Price" required name="price" type="number" id="price" className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <button className="btn btn-primary" onClick={notify}>Create</button>
                        <ToastContainer
                            position="top-right"
                            autoClose={2000}
                            hideProgressBar={false}
                            newestOnTop={false}
                            closeOnClick
                            rtl={false}
                            pauseOnFocusLoss
                            draggable
                            pauseOnHover={false}
                            theme="light"
                        />
                    </form>
                </div>
            </div>
        </div>
    )
}

export default SaleRecordForm;
