import React, { useEffect, useState } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function SalesPersonForm() {
  const notify = () => toast("Sales Person Added");

  const [formData, setFormData] = useState({
    name: '',
    employee_number: '',
  })

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8090/api/employees/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      }
    }

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        name: '',
        employee_number: '',
      })
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    })
  }
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a sales person</h1>
          <form onSubmit={handleSubmit} id="create-salesperson-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.employee_number} placeholder="Employee number" required type="number" name="employee_number" id="employee_number" className="form-control" />
              <label htmlFor="employee_number">Employee number</label>
            </div>
            <button className="btn btn-primary" onClick={notify}>Create</button>
            <ToastContainer
              position="top-right"
              autoClose={2000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover={false}
              theme="light"
            />
          </form>
        </div>
      </div>
    </div>
  )
}

export default SalesPersonForm;
