import React, { useState, useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function AppointmentForm() {
    const notify = () => toast("Appointment Created");

    const [techs, setTechs] = useState([])
    const [formData, setFormData] = useState({
        vin: '',
        customer_name: '',
        date: '',
        time: '',
        technician: '',
        reason: '',
    })

    const getData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechs(data.techs);
        }
    }

    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const autoUrl = 'http://localhost:8080/api/appointments/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(autoUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                vin: '',
                customer_name: '',
                date: '',
                time: '',
                technician: '',
                reason: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange}
                                value={formData.vin}
                                placeholder="vin"
                                required type="text"
                                name="vin" id="vin"
                                className="form-control" />
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange}
                                value={formData.customer_name}
                                placeholder="Customer Name"
                                required type="text"
                                name="customer_name" id="customer_name"
                                className="form-control" />
                            <label htmlFor="customer_name">Customer Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange}
                                value={formData.date}
                                placeholder="Date"
                                required type="text"
                                name="date" id="date"
                                className="form-control" />
                            <label htmlFor="date">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange}
                                value={formData.time}
                                placeholder="Time"
                                required type="text"
                                name="time" id="time"
                                className="form-control" />
                            <label htmlFor="time">Time</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange}
                                value={formData.technician}
                                required name="technician"
                                id="technician"
                                className="form-select">
                                <option value="">Choose a Technician</option>
                                {techs.map(tech => {
                                    return (
                                        <option key={tech.id} value={tech.id}>{tech.name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange}
                                value={formData.reason}
                                placeholder="Reason"
                                required type="text"
                                name="reason" id="reason"
                                className="form-control" />
                            <label htmlFor="reason">Reason</label>
                        </div>
                        <button className="btn btn-primary" onClick={notify}>Create</button>
                        <ToastContainer
                        position="top-right"
                        autoClose={2000}
                        hideProgressBar={false}
                        newestOnTop={false}
                        closeOnClick
                        rtl={false}
                        pauseOnFocusLoss
                        draggable
                        pauseOnHover={false}
                        theme="light"
                        />
                    </form>
                </div>
            </div>
        </div>
    );
}

export default AppointmentForm;
