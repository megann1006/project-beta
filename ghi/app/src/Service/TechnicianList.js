import React, { useEffect, useState } from 'react';

function TechnicianList() {
    const [techs, setTechs] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/');
        const data = await response.json();
        setTechs(data.techs)

    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <div>
            <h1>Technicians</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Employee Number</th>
                    </tr>
                </thead>
                <tbody>
                    {techs.map(tech => {
                        return (
                            <tr key={tech.id}>
                                <td>{tech.name}</td>
                                <td>{tech.employee_num}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default TechnicianList;
