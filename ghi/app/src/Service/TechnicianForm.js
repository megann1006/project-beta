import React, { useState, useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function TechnicianForm() {
    const notify = () => toast("Technician Added");

    const [formData, setFormData] = useState({
        name: '',
        employee_num: '',
    })

    const handleSubmit = async (event) => {
        event.preventDefault();

        const techUrl = 'http://localhost:8080/api/technicians/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(techUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
                employee_num: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add Technician</h1>
                    <form onSubmit={handleSubmit} id="create-manufacturer-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange}
                            value={formData.name}
                            placeholder="Name"
                            required type="text"
                            name="name" id="name"
                            className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange}
                            value={formData.employee_num}
                            placeholder="Employee Number"
                            required type="text"
                            name="employee_num" id="employee_num"
                            className="form-control" />
                            <label htmlFor="name">Employee Number</label>
                        </div>
                        <button className="btn btn-primary" onClick={notify}>Create</button>
                        <ToastContainer
                        position="top-right"
                        autoClose={2000}
                        hideProgressBar={false}
                        newestOnTop={false}
                        closeOnClick
                        rtl={false}
                        pauseOnFocusLoss
                        draggable
                        pauseOnHover={false}
                        theme="light"
                        />
                    </form>
                </div>
            </div>
        </div>
    );
}

export default TechnicianForm;
