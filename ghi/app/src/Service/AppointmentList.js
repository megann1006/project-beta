import React, { useEffect, useState } from 'react';

function AppointmentList() {
    const [appointments, setAppointments] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        const data = await response.json();
        setAppointments(data.appointments)
    }

    useEffect(() => {
        getData()
    }, [])

    const finishAppointment = (id) => {
        const updatedAppointments = appointments.filter(appointment => appointment.id !== id);
        setAppointments(updatedAppointments);
    }


    const cancelAppointment = async (id) => {
        await fetch(`http://localhost:8080/api/appointments/${id}`, {
            method: 'DELETE'
        });
        const updatedAppointments = appointments.filter(appointment => appointment.id !== id);
        setAppointments(updatedAppointments);
    }


    return (
        <div>
            <h1>Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIP</th>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointments => {
                        return (
                            <tr key={appointments.id}>
                                <td>{appointments.vip ? "✔" : "✘"}</td>
                                <td>{appointments.vin}</td>
                                <td>{appointments.customer_name}</td>
                                <td>{appointments.date}</td>
                                <td>{appointments.time}</td>
                                <td>{appointments.technician.name}</td>
                                <td>{appointments.reason}</td>
                                <td>
                                    <button className="btn btn-danger" onClick={() => cancelAppointment(appointments.id)}>Cancel</button>
                                    <button className="btn btn-success" onClick={() => finishAppointment(appointments.id)}>Finish</button>
                                </td>
                            </tr>
                );
                    })}
            </tbody>
        </table>
        </div >
    );
}

export default AppointmentList;
